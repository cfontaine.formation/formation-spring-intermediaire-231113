package fr.dawan.springboot.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.entities.relations.Article;
import fr.dawan.springboot.entities.relations.Emballage;
import fr.dawan.springboot.entities.relations.Marque;

//@Repository
@Transactional
public interface ArticleRepository extends JpaRepository<Article, Long> {

    List<Article> findByPrix(double prix);
    
    List<Article> findByPrixLessThan(double prixMax);
    
    List<Article> findByPrixGreaterThanAndEmballage(double prinMin,Emballage emballage);

    List<Article> findByPrixNot(double prix);
    
    List<Article> findByDescriptionLike(String modele);
    
    List<Article> findByPrixBetween(double prixMin,double prixMax);
    
    List<Article> findByPrixLessThanOrderByPrixDescDescription(double prixMax);
    
    List<Article> findByMarque(Marque m);
    
    // Expression de chemin ->uniquement avec les @OneToOne et les @ManyToOne
    List<Article>  findByMarqueNom(String nomMarque);
    
    List<Article>  findByMarqueDateCreationAfter(LocalDate date);
    
    // IgnoreCase
    List<Article>  findByMarqueNomIgnoreCase(String nomMarque);

    // sujet exists
    boolean existsByEmballage(Emballage emballage);
    
    // sujet count
    int countByPrixLessThan(double prixMin);
    
    // sujet delete -> retour void ou int 
    int deleteByMarque(Marque marque);
    
    // Top ou First -> en SQL LIMIT
    List<Article> findTop3ByOrderByPrixDesc();
    
    Article findTopByOrderByPrix(); // 1 seul objet
    
    // JPQL 
 //   @Query("SELECT a FROM Article a WHERE a.prix<:prix")
    @Query("FROM Article a WHERE a.prix<:prix")
    List<Article> findByPrixLessThanJPQL(@Param("prix")double prixMax);
    
    // Expression de chemin ->uniquement avec les @OneToOne et les @ManyToOne
    @Query("SELECT a FROM Article a WHERE a.marque.nom<:nomMarque")
    List<Article> findByMarqueNomJPQL(String nomMarque);
    
    // Jointure Interne
    @Query("SELECT a FROM Article a JOIN FETCH a.fournisseurs")
    List<Article> findByFournisseurJPQL();
    
    @Query("SELECT a FROM Article a JOIN FETCH a.fournisseurs f WHERE f.nom=?1")
    List<Article> findByFournisseurNomJPQL(String nomFournisseur);
    
    // Jointure Externe
    @Query("SELECT a FROM Article a LEFT JOIN FETCH a.fournisseurs f")
    List<Article> findByFournisseurLeftJPQL();
    
    // SQL
    @Query(value="SELECT * FROM articles WHERE prix<:prix",nativeQuery = true)
    List<Article> findByPrixLessThanSQL(@Param("prix")double prixMax);
    
    // Pagination
    List<Article> findAllBy(Pageable pageable);
    Slice<Article> findSliceAllBy(Pageable pageable);
}
