package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springboot.entities.relations.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {

    List<Marque> findByNomLike(String model);
    
    int removeById(long id);
    
}
