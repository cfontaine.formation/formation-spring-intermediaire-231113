package fr.dawan.springboot.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    
    @Value("${message.hello}")
    private String message;
    
    @RequestMapping("/hello")
    public String helloworld() {
        return message;
    }
}
