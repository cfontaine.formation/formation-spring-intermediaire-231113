package fr.dawan.springboot.controllers;

import java.sql.SQLException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.dawan.springboot.dto.ApiError;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<?> handleConflict(SQLException e,WebRequest request){
        ApiError err=new ApiError(HttpStatus.BAD_REQUEST, e.getMessage());
        return handleExceptionInternal(e, err,new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    
}
