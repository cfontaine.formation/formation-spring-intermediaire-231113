package fr.dawan.springboot.runners;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;

import lombok.extern.slf4j.Slf4j;
@Slf4j
//@Component
@Order(3)
public class LogRunner implements CommandLineRunner {

    // static final Logger log=LoggerFactory.getLogger(LogRunner.class);
    // peut-être généré avec l'annotation @Slf4j de lombok
    @Override
    public void run(String... args) throws Exception {
        log.trace("Message Trace");
        log.debug("Message Debug");
        log.info("Message Info");
        log.warn("Message Warning");
        log.error("Message Error");
    }

}
