package fr.dawan.springboot.runners;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;

import fr.dawan.springboot.entities.relations.Article;
import fr.dawan.springboot.entities.relations.Emballage;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.repositories.MarqueRepository;

//@Component
@Order(1)
public class RepositoryRunner implements CommandLineRunner {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private MarqueRepository marqueRepository;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Repository Runner");

        List<Article> lstA = articleRepository.findAll();

        for (Article a : lstA) {
            System.out.println(a + " " + a.getMarque());
        }

        lstA = articleRepository.findByPrix(30.0);
        for (Article a : lstA) {
            System.out.println(a);
        }

        lstA = articleRepository.findByDescriptionLike("%SSD%");
        for (Article a : lstA) {
            System.out.println(a);
        }

        articleRepository.findByPrixLessThan(100.0).forEach(m -> System.out.println(m));

        articleRepository.findByPrixGreaterThanAndEmballage(50.0, Emballage.CARTON).forEach(m -> System.out.println(m));

        articleRepository.findByPrixBetween(30.0, 300.0).forEach(m -> System.out.println(m));

        articleRepository.findByPrixLessThanOrderByPrixDescDescription(1000.0).forEach(m -> System.out.println(m));

        Marque mA = marqueRepository.findById(1L).get();

        articleRepository.findByMarque(mA).forEach(m -> System.out.println(m));

        articleRepository.findByMarqueNom("MarqueA").forEach(m -> System.out.println(m));

        articleRepository.findByMarqueDateCreationAfter(LocalDate.of(1970, 1, 1)).forEach(m -> System.out.println(m));

        articleRepository.findByMarqueNomIgnoreCase("MArQUEb").forEach(m -> System.out.println(m));

        System.out.println(articleRepository.existsByEmballage(Emballage.PAPIER));
        System.out.println(articleRepository.existsByEmballage(Emballage.CARTON));

        System.out.println(articleRepository.countByPrixLessThan(40.0));

        Marque mD = marqueRepository.findById(4L).get();

        articleRepository.deleteByMarque(mD);

        System.out.println(articleRepository.findTopByOrderByPrix());

        articleRepository.findTop3ByOrderByPrixDesc().forEach(m -> System.out.println(m));

        articleRepository.findByFournisseurJPQL().forEach(m -> {
            System.out.println(m);
            System.out.println(m.getFournisseurs().get(0));
        });

        articleRepository.findByFournisseurLeftJPQL().forEach(m -> {
            System.out.println(m);
            System.out.println(m.getFournisseurs());
        });//

        // Création d'un Pageable depuis PageRequest
        Pageable pageable = PageRequest.of(0, 2);
        articleRepository.findAllBy(pageable).forEach(System.out::println);
        System.out.println("Slice :");
        Slice<Article> sliceAllBy = articleRepository.findSliceAllBy(pageable);
        sliceAllBy.forEach(System.out::println);
        System.out.println("Page :");
        Page<Article> pageAll = articleRepository.findAll(pageable);
        pageAll.forEach(System.out::println);
        System.out.println("Sort :");
        // Sort permet de définir dynamiquement un ordre de tri pour les données
        Sort sort = Sort.by(Sort.Order.by("prix")); // Order peur être naturel (by), 'asc' ou 'desc'
        Pageable pageableWithSort = PageRequest.of(1, 2, sort);
        articleRepository.findAll(pageableWithSort).forEach(System.out::println);
        // Les méthodes qui attendent Pageable ou Sort n'acceptent pas de valeur null,
        // vous pouvez donc utiliser  Pageable.unpaged(), Sort.unsorted():
        articleRepository.findAll(Pageable.unpaged()).forEach(System.out::println);
    }
}
