package fr.dawan.springboot.runners;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.services.MarqueService;

//@Component
@Order(2)
public class ServiceRunner implements CommandLineRunner {

    @Autowired
    private MarqueService marqueService;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Service Runner");
        
        marqueService.getAllMarque(Pageable.unpaged()).forEach( s -> System.out.println(s));

        MarqueDto dto=marqueService.getMarqueById(2L);
        System.out.println(dto);
        
        marqueService.getMarqueByNom("%C").forEach( s -> System.out.println(s));
        
        MarqueDto md=new MarqueDto(0L,"Marque E",LocalDate.of(1998,7,13));
        MarqueDto mr=marqueService.saveOrUpdate(md);
        System.out.println(mr);
        
        System.out.println(marqueService.deleteMarque(3000L));
        
        System.out.println(marqueService.deleteMarque(mr.getId()));
    }

}
