package fr.dawan.springboot.entities.relations;

public enum Emballage {
    CARTON, PLASTIQUE, PAPIER, SANS
}
