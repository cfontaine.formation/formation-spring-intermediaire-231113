package fr.dawan.springboot.entities.heritage;

import java.io.Serializable;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@MappedSuperclass // -> Annotation placé sur la classe mère
                  // Aucune table ne correspondra à la classe mère dans la base de données
                  // L’état de la classe mère sera rendu persistant dans les tables associées à
                  // ses classes entités filles

public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;
}
