package fr.dawan.springboot.entities.relations;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "maires")
public class Maire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String prenom;

    private String nom;

    // @OneToOne => Relation 1,1
    // ici, Un maire n'est que d'une seul ville et une ville à seul maire

    // Relation @OneToOne Unidirectionnel
    // on a uniquement une relation Maire -> Ville
    // et pas de relation Ville -> Maire, on n'a pas accés au Maire depuis la Vil
    @OneToOne(mappedBy = "maire")
    @Exclude    // si on utilise @ToString, il faut ajouter l'annotation @Exclude pour les attribut qui sont des entitées. 
                // pour qu'ils ne soient pas ajouté dans la méthode toString -> cycles 
    private Ville ville;
}
