package fr.dawan.springboot.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.springboot.entities.heritage.AbstractEntity;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

//une entité doit :
//- être annoté avec @Entity
//- avoir un attribut qui représente la clé primaire annoté avec @Id
//- implémenté l'interface Serializable
//- avoir obligatoirement un contructeur par défaut

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "employes") // L'annotation @Table permet de modifier le nom de la table, sinon elle prend/
                          // le nom de la classe
public class Employe extends AbstractEntity/* implements Serializable */ {

    private static final long serialVersionUID = 1L;
 
//    id et version sont hérités de AbstractEntity 

//    @Id // -> Clé primaire simple
//    @GeneratedValue(strategy = GenerationType.AUTO)       // 1 ORM -> choisie la stratégie
//    @GeneratedValue(strategy = GenerationType.IDENTITY)   // 2 BDD -> auto_increment, Identity, ...

//    @TableGenerator(name = "employegen")
//    @GeneratedValue(strategy = GenerationType.TABLE, generator = "employegen") // 3 ORM

//    @SequenceGenerator(name = "employe_seq")              // 4 -> BDD séquence
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employe_seq")
//    private long id;

//     @Version // -> Gestion de la concurrence Optimistic
//     private int version;

    // L'annotation @Column permet pour définir plus précisément la colonne
    @Column(length = 50) // l'attribut length permet modifier de la longueur d'une chaine de caractère ou
                         // d'un @lob
    private String prenom; // Sinon par défaut 255 caractères ou octets

    // l'attribut nullable permet de définir si le champ peut être null (optionelle)
    // par défaut true
    @Column(length = 50, nullable = false)
    private String nom;

    @Column(nullable = false)
    private double salaire;

    @Column(nullable = false, unique = true)
    private String email;

    // LocalDate, LocalTime, LocalDateTime sont supportés par hibernate depuis la version 5
    @Column(name = "date_naissance")
    private LocalDate dateNaissance;

    // Une énumération peut être stocké sous forme numérique EnumType.ORDINAL (par défaut)
    // ou sous forme de chaine de caractères EnumType.STRING
    @Enumerated(EnumType.STRING)
    private Contrat contrat = Contrat.CDI;

    // @Lob => pour stocker des données binaires dans la bdd (image, ...) BLOB ou un long texte CLOB
    @Lob
    @Column(length = 65000) // l'attribut length permet de définir la taille du blob => ici un MediumBlob max 16 mo
    private byte[] photo; // pour MYSQL : par défaut un Tinyblob -> 255, Blob-> 65535, MediumBlob -> 16
                          // mo, LongBlob -> 4,29 Go
                          // BLOB -> tableau de byte , CLOB -> String ou tableau de caractère

    // @ Embedded => pour utiliser une classe intégrable
    @Embedded
    private Adresse adressePerso;

    // Pour utiliser plusieurs fois la même classe imbriqué dans la même entitée
    // On aura plusieurs fois le même nom de colonne dans la table => erreur
    // On pourra renommé les colonnes avec des annotations @AttributeOverride placé
    // dans une annotation @AttributeOverrides
    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "ville", column = @Column(name = "ville_pro")),
            @AttributeOverride(name = "rue", column = @Column(name = "rue_pro")),
            @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_pro", length = 20)) })
    private Adresse adressePro;

    // Les attributs @Transient ne sont transient ne sont pas persister
    // sinon tous les autres sont par défaut persistant
    @Transient
    private int nePasPersister;

    // Mapping d'une collection simple (Integer, String ...)
    @ElementCollection  // @ElementCollection -> par défaut, génére une table NomClasse_nomVariable
    // @CollectionTable -> permet de personaliser de la table name: nom de la table, joinColumns -> nom de la colonne de jointure 
    @CollectionTable(name = "telephones", joinColumns = @JoinColumn(name = "id_employe"))
    @Column(name = "telephone") // personaliser le nom de la colonne contenant les données contenues dans la collection
    private List<String> telephones = new ArrayList<>();
}
