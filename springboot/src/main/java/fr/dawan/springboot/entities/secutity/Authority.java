package fr.dawan.springboot.entities.secutity;

import org.springframework.security.core.GrantedAuthority;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class Authority implements GrantedAuthority {

    private static final long serialVersionUID = 1L;

    @Id
    private String authority;

    @Override
    public String getAuthority() {
        return authority;
    }

}
