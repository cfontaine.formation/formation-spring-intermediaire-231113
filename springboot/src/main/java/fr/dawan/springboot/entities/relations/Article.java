package fr.dawan.springboot.entities.relations;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="articles")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable = false)
    private String description;
    
    @Column(nullable = false)
    private double prix;
    
    @Enumerated(EnumType.STRING)
    private Emballage emballage;
    
    
    @ManyToOne
    @Exclude
    //@JoinColumn(name = "fk_marque")
    private Marque marque; 
    
    @ManyToMany
    @Exclude
//    @JoinTable(name="fournisseur2aticle",
//    joinColumns = @JoinColumn(name="fk_article"),
//    inverseJoinColumns = @JoinColumn(name="fk_fournisseur"))
    private List<Fournisseur> fournisseurs=new ArrayList<>();

}
