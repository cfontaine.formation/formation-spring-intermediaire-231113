package fr.dawan.springboot.entities.heritage;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="compte_bancaire")

//Héritage => trois façons d’organiser l’héritage

//1 - SINGLE_TABLE => Le parent et les enfants vont être placé dans la même table
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//Une colonne "Discriminator" définit le type de la classe enregistrée
@DiscriminatorColumn(name = "compte_discriminator")
//Pour la classe CompteBancaire la valeur dans la colonne discriminator => CB
@DiscriminatorValue("COMPTE")

//2 - TABLE_PER_CLASS => le parent et chaque enfant auront leur propre table
//la table enfant aura les colonnes des variables d'instance de la classe parent
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

//3 - JOINED => on aura une jointure entre la table de la classe parent et la table de la classe enfant
//@Inheritance(strategy = InheritanceType.JOINED)

public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO) // GenerationType.IDENTITY ne fonction avec TABLE_PER_CLASS
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable=false)
    private double solde;
    
    @Column(nullable=false,length=60)
    private String titulaire;
    
    @Column(nullable=false,length=40)
    private String iban;
}
