package fr.dawan.springboot.entities.heritage;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
//pour SINGLE_TABLE
//Pour la classe CompteEpargne la valeur dans la colonne discriminator => COMPTE_EPARGNE

@DiscriminatorValue("COMPTE_EPARGNE")
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;

    @Column(name = "taux_interet")
    private double tauxInteret;
}
