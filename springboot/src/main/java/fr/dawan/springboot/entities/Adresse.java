package fr.dawan.springboot.entities;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

//Une classe intégrable va stocker ses données dans la table de l’entité mère ce qui va créer des colonnes supplémentaires

//On annote une classe intégrable avec @Embeddable
@Embeddable
public class Adresse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String rue;

    private String ville;

    @Column(name = "code_postal", length = 20)
    private String codePostal;
}
