package fr.dawan.springboot.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dto.ArticleDto;
import fr.dawan.springboot.entities.relations.Article;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.services.ArticleService;

@Service
@Transactional(readOnly = true)
public class ArticleServiceImpl extends GenericServiceImpl<ArticleDto,Article,Long> implements ArticleService {

    public ArticleServiceImpl(ArticleRepository repository, ModelMapper mapper) {
        super(repository, mapper, Article.class, ArticleDto.class);
    }

 }

