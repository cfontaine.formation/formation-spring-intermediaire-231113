package fr.dawan.springboot.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenericService<TDto,ID> {
    
    Page<TDto> getALL(Pageable page);
    
    TDto getById(ID id);
    
    boolean delete(ID id);
    
    TDto saveOrUpdate(TDto dto);
}

