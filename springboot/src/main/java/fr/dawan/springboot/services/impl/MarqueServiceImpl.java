package fr.dawan.springboot.services.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.MarqueService;

@Service
@Transactional
public class MarqueServiceImpl implements MarqueService {

    @Autowired
    private MarqueRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<MarqueDto> getAllMarque(Pageable page) {
        List<Marque> lstMarque = repository.findAll(page).getContent();

//        List<MarqueDto> lstDto=new ArrayList<>();
//        for(Marque m: lstMarque) {
//            MarqueDto dto=mapper.map(m, MarqueDto.class);
//            lstDto.add(dto);
//        }
//        return lstDto;
        return lstMarque.stream().map(m -> mapper.map(m, MarqueDto.class)).toList();

    }

    @Override
    public MarqueDto getMarqueById(long id) {
        Optional<Marque> marque = repository.findById(id);
        return mapper.map(marque.get(), MarqueDto.class);
    }

    @Override
    public List<MarqueDto> getMarqueByNom(String nom) {
        List<Marque> lstMarque = repository.findByNomLike(nom);
        return lstMarque.stream().map(m -> mapper.map(m, MarqueDto.class)).toList();
    }

    @Override
    public boolean deleteMarque(long id) {
        return repository.removeById(id) != 0;
    }

    @Override
    public MarqueDto saveOrUpdate(MarqueDto marqueDto) {
        // Convertion entité -> DTO
        Marque m = mapper.map(marqueDto, Marque.class);
        Marque mr = repository.saveAndFlush(m);
        // Convertion DTO -> entité
        return mapper.map(mr, MarqueDto.class);
    }

}
