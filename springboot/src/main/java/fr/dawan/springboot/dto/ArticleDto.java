package fr.dawan.springboot.dto;

import fr.dawan.springboot.entities.relations.Emballage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class ArticleDto {

    private long id;
    
    private double prix;
    
    private Emballage emballage;
    
    private MarqueDto marque;
}

