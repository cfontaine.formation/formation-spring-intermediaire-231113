package fr.dawan.springboot.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class MarqueDto {

    private long id;
    
    private String nom;
    
    // @JsonIgnore
    // @JsonProperty("creation")
    // @JsonFormat(pattern = "dd-MM-YYYY",shape =Shape.STRING)
    private LocalDate dateCreation;
    
}
