package fr.dawan.bibliotheque.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "nations")
public class Nation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(length = 100, nullable = false)
    private String nom;
    
    @OneToMany(mappedBy = "nationalite")
    @Exclude
    private List<Auteur> auteurs=new ArrayList<>();

    public Nation(String nom) {
        this.nom = nom;
    }
}