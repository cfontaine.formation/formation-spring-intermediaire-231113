package fr.dawan.bibliotheque.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "livres")
public class Livre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(nullable = false)
    private String titre;

    @Column(name="annee_sortie",nullable = false)
    private int anneeSortie;
    
    @ManyToOne
    @Exclude
    private Categorie categorie;

    @ManyToMany
    @Exclude
    private List<Auteur> auteurs=new ArrayList<>();
    
    public Livre(String titre, int anneeSortie) {
        this.titre = titre;
        this.anneeSortie = anneeSortie;
    }

}


