package fr.dawan.bibliotheque.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "auteurs")
public class Auteur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(length = 50, nullable = false)
    private String prenom;

    @Column(length = 50, nullable = false)
    private String nom;

    @Column(nullable = false)
    private LocalDate naissance;

    private LocalDate deces;
    
    @ManyToOne
    @Exclude
    private Nation nationalite;
    
    @ManyToMany(mappedBy="auteurs")
    @Exclude
    private List<Livre> livres=new ArrayList<>();

    public Auteur(String prenom, String nom, LocalDate naissance, LocalDate deces) {
        this.prenom = prenom;
        this.nom = nom;
        this.naissance = naissance;
        this.deces = deces;
    }
}


