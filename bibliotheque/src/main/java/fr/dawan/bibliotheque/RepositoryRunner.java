package fr.dawan.bibliotheque;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import fr.dawan.bibliotheque.repositories.AuteurRepository;
import fr.dawan.bibliotheque.repositories.LivreRepository;

@Component
public class RepositoryRunner implements CommandLineRunner {

    @Autowired
    AuteurRepository auteurRepository;

    @Autowired
    LivreRepository livreRepository;

    @Override
    public void run(String... args) throws Exception {
        auteurRepository.findByDecesIsNull(PageRequest.of(0, 5)).forEach(System.out::println);

        auteurRepository.findByNomIgnoreCase("king").forEach(System.out::println);

        auteurRepository.findByLivreId(1L).forEach(System.out::println);

        auteurRepository.findByTopAuteurNombreLivre().forEach(System.out::println);

        System.out.println("--------------------------------------");

        livreRepository.findByTitreLike("D__%").forEach(System.out::println);

        System.out.println(livreRepository.findByAnneeSortie(1992));

        livreRepository.findByAnneeSortieBetween(1960, 1970).forEach(System.out::println);

        livreRepository.findByCategorieNomOrderByAnneeSortieDesc("Policier").forEach(System.out::println);

    }

}
