package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.bibliotheque.entities.Nation;

public interface NationRepository extends JpaRepository<Nation, Long> {

    List<Nation> findByNom(String nom);
}
