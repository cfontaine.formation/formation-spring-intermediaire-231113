package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.bibliotheque.entities.Livre;

public interface LivreRepository extends JpaRepository<Livre, Long> {
    // pour faire un recherche avec l'opérateur Like sur les titres
    List<Livre> findByTitreLike(String motif);

    // qui retourne tous les livres sorties pour une année passer en paramètre
    List<Livre> findByAnneeSortie(int annee);

    // qui retourne tous les livres sorties pour un intervalle d'année min et max
    // passé en paramètres
    List<Livre> findByAnneeSortieBetween(int anneeMin, int anneeMax);

    // tous les livres d'une categorie trié par année de sortie décroissant, on
    // passe en paramètre le nom de la categorie
    List<Livre> findByCategorieNomOrderByAnneeSortieDesc(String nomCategeorie);
}
