package fr.dawan.bibliotheque.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.bibliotheque.entities.Categorie;

public interface CategoryRepository extends JpaRepository<Categorie, Long> {

}
