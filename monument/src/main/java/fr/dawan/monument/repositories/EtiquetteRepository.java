package fr.dawan.monument.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.monument.entities.Etiquette;

public interface EtiquetteRepository extends JpaRepository<Etiquette, Long> {
    List<Etiquette> findByIntituleLike(String modele);

    int removeById(long id);
}
