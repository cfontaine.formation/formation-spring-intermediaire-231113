package fr.dawan.monument.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.dawan.monument.entities.Monument;

public interface MonumentRepository extends JpaRepository<Monument, Long> {

    List<Monument> findByAnneeConstructionBetweenOrderByAnneeConstructionDesc(int anneeMin, int anneeMax);

    List<Monument> findByNomLike(String modele);

    Monument findByCoordoneLatitudeAndCoordoneLongitude(double latitude, double longitude);

    List<Monument> findByLocalisationPaysIn(List<String> pays, Pageable page);

    List<Monument> findTop5ByOrderByAnneeConstruction();

    @Query("FROM Monument m JOIN m.etiquettes e WHERE e.intitule=:intitule")
    List<Monument> findByEtiquetteIntitule(String intitule);

}
