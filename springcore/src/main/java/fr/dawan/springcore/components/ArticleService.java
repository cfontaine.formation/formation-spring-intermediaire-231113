package fr.dawan.springcore.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service(value = "service1") // @Service, @Repository, @Controller , @Composant => un bean est créé

public class ArticleService {
    @Autowired 
    @Qualifier("repository1")
    // injection automatique de la dépendence. Un bean de type ArticleRepository est recherché dans le conteneur d'ioc
    // et il est injecté dans la variable d'instance repository 
    // s'il y a plusieurs bean une exception est générée, à moins de lever l'ambiguité avec @Primary ou @Qualifier

    // required = false-> dépendence optionnelle: s'il n'y a pas de bean de type ArticleRepository dans le conteneur -> repository= null et pas d'exception

    private ArticleRepository repository;

    public ArticleService() {
        System.out.println("Constructeur par défaut");
    }

    // @Autowired
    public ArticleService(/* @Qualifier("repository1") */ ArticleRepository repository) {
        System.out.println("Constructeur un paramètre");
        this.repository = repository;
    }

    public ArticleRepository getRepository() {
        return repository;
    }

    // @Autowired
    public void setRepository(/* @Qualifier("repository2") */ ArticleRepository repository) {
        System.out.println("Setter Service");
        this.repository = repository;
    }

    @Override
    public String toString() {
        return "ArticleService [repository=" + repository + "]";
    }

}
