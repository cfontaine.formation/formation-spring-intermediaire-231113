package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.DtoMapper;
import fr.dawan.springcore.components.ArticleRepository;
import fr.dawan.springcore.components.ArticleService;

public class App {
    public static void main(String[] args) {
        // Exemple: Lombok
//        Article a=new Article("stylo",2.0);
//        System.out.println(a);

        // Exemple: spring core
        // Création du conteneur d'ioc
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConf.class);
        System.out.println("----------------------------------------------------");
        DtoMapper m1 = ctx.getBean("mapper2", DtoMapper.class);
        System.out.println(m1);

        // getBean -> permet de récupérer les instances des beans depuis le conteneur
        ArticleRepository repo1 = ctx.getBean("repository1", ArticleRepository.class);
        System.out.println("repository 1" + repo1);

        ArticleRepository repo2 = ctx.getBean("repository2", ArticleRepository.class);
        System.out.println("repository 2" + repo2);

        ArticleService s1 = ctx.getBean("service1", ArticleService.class);
        System.out.println(s1);

        // Fermeture du context entraine la destruction de tous les beans
        ((AbstractApplicationContext) ctx).close();
    }
}
